# /usr/bin/env python

# ==================================================================
#
# Script description:
#   This script is an example of how to zoom in and out in the URSINA game engine
#   using the ORTHOGRAPHIC camera for 2D scenery
#
#   Zoom in / out with the mouse scroll wheel
#   During the zoom out, the camera is kept centered on the same point before and after the zoom
#   During zoom in, the camera is centered on the point under the mouse cursor
#
# Author:
#   Michal Schorm
#
# Date:
#   11/2023
#
# ==================================================================

from ursina import *

import logging

# ==================================================================

# Set up logging
logging.basicConfig(level=logging.DEBUG)  # Set the logging level to DEBUG
logger = logging.getLogger("MyApp")  # Create a logger instance with the name "MyApp"

# Create an Ursina application
#   Intentionally set the app window width greate than height to demonstrate it's working properly
app = Ursina(title="Example app - scroll in / out", fullscreen=False, borderless=False, development_mode=False, size=Vec2(1200,600))

# Orthographic projection eliminate all perspective - this is what I want for 2D graphic with top-down view
camera.orthographic = True
# Moreover, for the orthographic camera, the 'camera.distance(0,0,-20)', does not have any effect, and the
# distance of the camera from the scene is constrolled by setting of the 'camera.fov', which in the specific
# case of orthographic camera means setting how many units *VERTICALLY* the camera can see
#   So in a app window which is 10:5, and FOV 10, there will be 10 units vertically and 20 horizontally.
# Source:
#  https://www.ursinaengine.org/platformer_tutorial.html
#  the "Positioning the camera" paragraph
camera.fov = 25

# ==================================================================

def camera_zoom(delta):

    # Number of units shown in the respective zoom setting
    MAX_ZOOM_IN = 1
    MAX_ZOOM_OUT = 100

    logger.debug("ZOOM DELTA PRE:  %s", delta)
    # Don't try to zoom in / out when we reached hard limit
    if delta > 0:
        delta = clamp( delta, 0, camera.fov - MAX_ZOOM_IN )
    elif delta < 0:
        delta = clamp( delta, camera.fov - MAX_ZOOM_OUT, 0 )
    logger.debug("ZOOM DELTA POST: %s", delta)

    # the '- delta' reverses the value. So when the delta value is positive, we are zooming IN,
    # when the delta value is negative, we are zooming OUT
    camera.fov = camera.fov - delta
    logger.debug("NEW CAMERA.FOV: %s", camera.fov)

    # Only when zooming IN
    if delta > 0:
        # Center the camera on a point under the cursor, when zooming
        camera.position += ( delta * mouse.x, delta * mouse.y, 0)
        logger.debug("NEW CAMERA.POSITION: %s", camera.position)

# ==================================================================

def input(key):
    logger.debug("KEY: %s", key)

    if key == "scroll up":
        camera_zoom(2)
    if key == "scroll down":
        camera_zoom(-2)

# ==================================================================

# Draw a grid so we can try that the zooming in / out works properly

# Set grid parameters
grid_spacing = 1
grid_size = 20
# Draw vertical lines
for i in range(-grid_size // 2, grid_size // 2 + 1, grid_spacing):
    line = Entity(model='quad', position=(i, 0), scale=(0.1, grid_size), color=color.white)
# Draw horizontal lines
for i in range(-grid_size // 2, grid_size // 2 + 1, grid_spacing):
    line = Entity(model='quad', position=(0, i), scale=(grid_size, 0.1), color=color.white)

# ==================================================================

app.run()
